#Statistical analyses of the phage and host transcriptomes - Cristina Howard-Varona 

setwd("Directory1/Directory1.1/") #Set your directory to where the RNA-seq read counts are


#0. Obtain the names of the genes for the phage and the host (it doesn't matter what file we take to do that, as the gene names are the same in all files!)
	Allfiles<-list.files("./Read_counts/", pattern="csv")
	pathToFiles1<-paste("./Read_counts/", Allfiles[1], sep="/")  
	hostgenes<-read.delim(pathToFiles1, sep=",", header=T, stringsAsFactors=F)  #this is to be able to open and read the file
	Hostgenenames<-hostgenes[,1] # this gives us the names of bacterial names; it is selecting for all rows (,) and column 1
	
	Allfiles2<-list.files("./Read_counts/PhageTo38/", pattern="csv")
	pathToFiles2<-paste("./Read_counts/PhageTo38/", Allfiles2[1], sep="/")
	phagegenes<-read.delim(pathToFiles2, sep=",", header=T, stringsAsFactors=F)
	Phagegenenames<-phagegenes[,1] #it's ok to have 1 because phage and host are in different folders

	Phage_NCBI<-read.csv("Directory1/Directory1.1/nameOfFile.csv", sep=",", header=T, stringsAsFactors=F)
	Phage_NCBI<-as.matrix(Phage_NCBI)
	Phagegenenames_NCBI<-Phage_NCBI[,2]
	
	
#1. Matrix with read counts: host and phage will have both control and infected 
	numHostgenes<-length(Hostgenenames)
	MatrixCountsHost<-matrix(nrow=numHostgenes,ncol=36)
	rownames(MatrixCountsHost)<-Hostgenenames

	numPhagegenes<-length(Phagegenenames)
	MatrixCountsPhage<-matrix(nrow=numPhagegenes,ncol=36) #36 is the number of samples
	rownames(MatrixCountsPhage)<-Phagegenenames

   #1.1. Obtain the name of the samples (e.g. 15 min I R3) to be then the name of the columns of our read count matrix
   cb<-1
   ExperConditionsList<-vector(mode="logical", length=36) #34 is the number of phage samples 
   for(i in seq(1,length(Allfiles),1)){ 
   ExperConditions<-strsplit(Allfiles[i], "_") #it's going to take the name of the file at each iteration
   ExperConditions<-sapply(ExperConditions,"[",1)
   ExperConditionsList[cb]<-c(ExperConditions)
   cb<-cb+1
   }
   colnames(MatrixCountsHost)<-c(ExperConditionsList) #these experimental conditions are not ordered because they have T120 next to T0
   colnames(MatrixCountsPhage)<-c(ExperConditionsList)
	
   #1.2. Loop to fill in the host's read counts
    cc<-1
    for (i in seq(1, length(Allfiles), 1)){   
    hostgenes<-read.delim(paste("./Read_counts/",Allfiles[i],sep="/"),header=T,sep=",",stringsAsFactors=F) #reads into the first file of Allfiles[1]
    MatrixCountsHost[,cc]<-c(hostgenes[,5])  #col 5 is where the read counts are
    cc<-cc+1
    }
    write.csv(MatrixCountsHost, "Directory1/Directory1.1/nameOfFile.csv")
     
     
   #1.3. Loop to fill in the phage's read counts
   cd<-1
   for (n in seq(1, length(Allfiles2), 1)){   #this is for when only the phage read counts are in that folder
   phagegenes<-read.delim(paste("./Read_counts/PhageTo38/",Allfiles2[n],sep="/"),header=T,sep=",",stringsAsFactors=F)
   MatrixCountsPhage[,cd]<-c(phagegenes[,6])
   cd<-cd+1
   }
   write.csv(MatrixCountsPhage, "Directory1/Directory1.1/nameOfFile.csv")
    


#2. Obtain a matrix with the length of the phage and host genes, for the log2RPKM done later
	Allfiles<-list.files("./Read_counts/", pattern="csv")
	pathToFiles1<-paste("./Read_counts/", Allfiles[1], sep="/")  
	hostgenes<-read.delim(pathToFiles1, sep=",", header=T, stringsAsFactors=F)  
	lenHost<-c(hostgenes[,6]) #the length of each gene is found in column 6 for the host
   
	Allfiles2<-list.files("./Read_counts/PhageTo38/", pattern="csv")
	pathToFiles2<-paste("./Read_counts/PhageTo38/", Allfiles2[1], sep="/")
	phagegenes<-read.delim(pathToFiles2, sep=",", header=T, stringsAsFactors=F)
	lenPhage<-c(phagegenes[,7])

   
   
#3. Host and phage matrix counts 
	#3.1. Host
		MatrixCountsHost_removed_ordered<-read.csv("Directory1/Directory1.1/nameOfFile.csv",sep=",", header=T, stringsAsFactors=F, row.names=1)
		MatrixCountsHost_removed_ordered<-as.matrix(MatrixCountsHost_removed_ordered)
	
		MatrixCountsHost_removed_ordered_C<-read.csv("Directory1/Directory1.1/nameOfFile.csv",sep=",", header=T, stringsAsFactors=F, row.names=1)
		MatrixCountsHost_removed_ordered_C<-as.matrix(MatrixCountsHost_removed_ordered_C)
		
	#3.2. Phage
		MatrixCountsPhage_removed_ordered_I<-read.csv("Directory1/Directory1.1/nameOfFile.csv",sep=",", header=T, stringsAsFactors=F, row.names=1)
		MatrixCountsPhage_removed_ordered_I<-as.matrix(MatrixCountsPhage_removed_ordered_I)
		
		
		
		
		## ********      Normalization of the read counts, calculation of RPKM       ********      
		
#4. Normalize the read counts matrices 
	
	library (edgeR)
	#4.1. Normalization of host I and C
		y<-DGEList(counts=MatrixCountsHost_removed_ordered,group=c("C0","C0","C0","I0","I0","C15","C15","C15","I15","I15","C30","C30","C30","I30","I30","C45","C45","C45","I45","I45","I45","C60","C60","C60","I60","I60","I60","C120","C120","I120","I120","I120"))
		y<-calcNormFactors(y)        
		y<-estimateCommonDisp(y)  
		y<-estimateCommonDisp(y)     
		y<-estimateTrendedDisp(y)
		y<-estimateTagwiseDisp(y) 
	
	#4.2. Normalization of host C
		w<-DGEList(counts=MatrixCountsHost_removed_ordered_C,group=c("C0","C0","C0","C15","C15","C15","C30","C30","C30","C45","C45","C45","C60","C60","C60","C120","C120"))
		w<-calcNormFactors(w)        
		w<-estimateCommonDisp(w)  
		w<-estimateCommonDisp(w)     
		w<-estimateTrendedDisp(w)
		w<-estimateTagwiseDisp(w) 
	
	#4.3. Normalization of phage infected
		a<-DGEList(counts=MatrixCountsPhage_removed_ordered_I,group=c("I0","I0","I15","I15","I30","I30","I45","I45","I45","I60","I60","I60","I120","I120","I120"))   
		a<-calcNormFactors(a)       
		a<-estimateCommonDisp(a)  
		a<-estimateCommonDisp(a)      
		a<-estimateTrendedDisp(a)
		a<-estimateTagwiseDisp(a) 
  
  
#5. log2 RPKM with final normalized samples 
		
	#5.1. Phage I:
		a<-as.matrix(a)
		RPKM_Phage_removed_norm<-rpkm(a, gene.length=lenPhage, normalized.lib.sizes=T, log=T, prior.count=0.25)
		write.csv(RPKM_Phage_removed_norm, "Directory1/Directory1.1/nameOfFile.csv")
				
		
			
			
## ********      Sample clustering       ********      		
			
#6. Dendrograms with the final, removed samples
	# Host:
		library(pvclust)
		pv_host_log2RPKM_final<-pvclust(RPKM_Host_removed_norm, method.dist="correlation", method.hclust="complete", nboot=10000) #it will create a dendrogram with samples hierarchically clustered, and 2 types of pvalues (red one we want it to be closes to 100% of supported data). Larger nboot reduces the pvalue
		pdf("Directory1/Directory1.1/nameOfFile.pdf")
		plot(pv_host_log2RPKM_final)
		dev.off()
		
		#Pheatmap correlation clustering gives the same dendrogram as pvclust:
		library(pheatmap)
		library(RColorBrewer)
		pal_YOR<-brewer.pal(9, "YlOrRd") #have one homogeneous color
		pdf("Directory1/Directory1.1/nameOfFile.pdf")
		pheatmap(RPKM_Host_removed_norm, pal_YOR,kmeans_k= NA, cluster_rows=F, cluster_cols=T,fontsize_row=5, border_color=NA, fontsize_col=10,clustering_distance_cols="correlation")
		dev.off()
			

			
			
## ********     Temporal expression clusters      ********    
			
#7. Number of expression clusters that the phage genes fit in according to their temporal expression
	RPKM_Phage_removed_norm<-read.csv("Directory1/Directory1.1/nameOfFile.csv",  sep=",", header=T, stringsAsFactors=F, row.names=1)
	RPKM_Phage_removed_norm<-as.matrix(RPKM_Phage_removed_norm)
					
	#Transpose the matrix
		RPKM_Phage_removed_norm_transposed<-t(RPKM_Phage_removed_norm)
	
	#Standarize the data
		scaledRPKM_phage_transposed<-scale(RPKM_Phage_removed_norm_transposed) 
		scaledRPKM_phage_transposed<-t(scaledRPKM_phage_transposed) #transpose back to have genes in rows
		write.csv(scaledRPKM_phage_transposed,"Directory1/Directory1.1/nameOfFile.csv")
	
	#Obtain how many clusters the data fit in
		library(clusterStab)
		hh<-benhur(scaledRPKM_phage_transposed, freq=0.7, upper=5, distmeth="pearson") 
		pdf("Directory1/Directory1.1/nameOfFile.pdf")
		hist(hh) #shows the genes in clusters, like a histogram
		dev.off()
		
		pdf("Directory1/Directory1.1/nameOfFile.pdf")
		ecdf(hh) #shows the Jaccard coefficient (how similar are the clusters) for the genes in the different clusters. 
		dev.off()
		
		clusterComp(scaledRPKM_phage_transposed,3) #estimates microarray cluster stability. 
	
		kmeanstable<-kmeans(scaledRPKM_phage_transposed, 3) #return which of the three clusters each gene belongs to
		KmeansForPhageGenes<-kmeanstable$cluster 
		



## ********      Differential expression (DE) analyses      ********    
		  
#8. DE analyses

  #8.1 Differential expression between I vs C (for the host):
   library (edgeR)
   
   zero<-exactTest(y,pair=c("C0","I0"))
   TTzero<-topTags(zero, 5000) ##calculates FDR for every condition (need to specify a number or it will give only the first 10)
   TTzero<-as.data.frame(TTzero)
   
   fifteen<-exactTest(y,pair=c("C15","I15"))
   TTfifteen<-topTags(fifteen, 5000)
   TTfifteen<-as.data.frame(TTfifteen)
   
   thirty<-exactTest(y,pair=c("C30","I30"))
   TTthirty<-topTags(thirty, 5000)
   TTthirty<-as.data.frame(TTthirty)

   fortyfive<-exactTest(y,pair=c("C45","I45"))
   TTfortyfive<-topTags(fortyfive, 5000)
   TTfortyfive<-as.data.frame(TTfortyfive)

   sixty<-exactTest(y,pair=c("C60","I60"))
   TTsixty<-as.data.frame(topTags(sixty, 5000))
   
   hundredtwenty<-exactTest(y,pair=c("C120","I120"))
   TThundredtwenty<-as.data.frame(topTags(hundredtwenty, 5000))
   
 #8.2. Differential expression between time points (for the phage and the host):
      #8.2.1.1 Host I vs I at all time points
        Host_0vs15<-exactTest(y,pair=c("I0","I15"))
        tags0vs15_Host<-as.data.frame(topTags(Host_0vs15, 5000))
		Host_0vs30<-exactTest(y,pair=c("I0","I30"))
		tags0vs30_Host<-as.data.frame(topTags(Host_0vs30, 5000))
		Host_0vs45<-exactTest(y,pair=c("I0","I45"))
		tags0vs45_Host<-as.data.frame(topTags(Host_0vs45, 5000))
		Host_0vs60<-exactTest(y,pair=c("I0","I60"))
		tags0vs60_Host<-as.data.frame(topTags(Host_0vs60, 5000))
		Host_0vs120<-exactTest(y,pair=c("I0","I120"))
		tags0vs120_Host<-as.data.frame(topTags(Host_0vs120, 5000))
		
		Host_15vs30<-exactTest(y,pair=c("I15","I30"))
		tags15vs30_Host<-as.data.frame(topTags(Host_15vs30, 5000))
		Host_15vs45<-exactTest(y,pair=c("I15","I45"))
		tags15vs45_Host<-as.data.frame(topTags(Host_15vs45, 5000))
		Host_15vs60<-exactTest(y,pair=c("I15","I60"))
		tags15vs60_Host<-as.data.frame(topTags(Host_15vs60, 5000))
		Host_15vs120<-exactTest(y,pair=c("I15","I120"))
		tags15vs120_Host<-as.data.frame(topTags(Host_15vs120, 5000))
		
		Host_30vs45<-exactTest(y,pair=c("I30","I45"))
		tags30vs45_Host<-as.data.frame(topTags(Host_30vs45, 5000))
		Host_30vs60<-exactTest(y,pair=c("I30","I60"))
		tags30vs60_Host<-as.data.frame(topTags(Host_30vs60, 5000))
		Host_30vs120<-exactTest(y,pair=c("I30","I120"))
		tags30vs120_Host<-as.data.frame(topTags(Host_30vs120, 5000))
		
		Host_45vs60<-exactTest(y,pair=c("I45","I60"))
		tags45vs60_Host<-as.data.frame(topTags(Host_45vs60, 5000))
		Host_45vs120<-exactTest(y,pair=c("I45","I120"))
		tags45vs120_Host<-as.data.frame(topTags(Host_45vs120, 5000))
		
		Host_60vs120<-exactTest(y,pair=c("I60","I120"))
		tags60vs120_Host<-as.data.frame(topTags(Host_60vs120, 5000))

	  #8.2.1.2 Host C vs C at all time points (to see if the host changes with time, even though there is no 'stress' to it)
		Host_0vs15<-exactTest(w,pair=c("C0","C15"))
        tags0vs15_Host<-as.data.frame(topTags(Host_0vs15, 5000))
		Host_0vs30<-exactTest(w,pair=c("C0","C30"))
		tags0vs30_Host<-as.data.frame(topTags(Host_0vs30, 5000))
		Host_0vs45<-exactTest(w,pair=c("C0","C45"))
		tags0vs45_Host<-as.data.frame(topTags(Host_0vs45, 5000))
		Host_0vs60<-exactTest(w,pair=c("C0","C60"))
		tags0vs60_Host<-as.data.frame(topTags(Host_0vs60, 5000))
		Host_0vs120<-exactTest(w,pair=c("C0","C120"))
		tags0vs120_Host<-as.data.frame(topTags(Host_0vs120, 5000))
		
		Host_15vs30<-exactTest(w,pair=c("C15","C30"))
		tags15vs30_Host<-as.data.frame(topTags(Host_15vs30, 5000))
		Host_15vs45<-exactTest(w,pair=c("C15","C45"))
		tags15vs45_Host<-as.data.frame(topTags(Host_15vs45, 5000))
		Host_15vs60<-exactTest(w,pair=c("C15","C60"))
		tags15vs60_Host<-as.data.frame(topTags(Host_15vs60, 5000))
		Host_15vs120<-exactTest(w,pair=c("C15","C120"))
		tags15vs120_Host<-as.data.frame(topTags(Host_15vs120, 5000))
		
		Host_30vs45<-exactTest(w,pair=c("C30","C45"))
		tags30vs45_Host<-as.data.frame(topTags(Host_30vs45, 5000))
		Host_30vs60<-exactTest(w,pair=c("C30","C60"))
		tags30vs60_Host<-as.data.frame(topTags(Host_30vs60, 5000))
		Host_30vs120<-exactTest(w,pair=c("C30","C120"))
		tags30vs120_Host<-as.data.frame(topTags(Host_30vs120, 5000))
		
		Host_45vs60<-exactTest(w,pair=c("C45","C60"))
		tags45vs60_Host<-as.data.frame(topTags(Host_45vs60, 5000))
		Host_45vs120<-exactTest(w,pair=c("C45","C120"))
		tags45vs120_Host<-as.data.frame(topTags(Host_45vs120, 5000))
		
		Host_60vs120<-exactTest(w,pair=c("C60","C120"))
		tags60vs120_Host<-as.data.frame(topTags(Host_60vs120, 5000))

	  		
	  #8.2.2. Phage I vs I at all time points
		Phage_0vs15<-exactTest(a,pair=c("I0","I15"))
        tags0vs15_Phage<-as.data.frame(topTags(Phage_0vs15, 5000))
		Phage_0vs30<-exactTest(a,pair=c("I0","I30"))
		tags0vs30_Phage<-as.data.frame(topTags(Phage_0vs30, 5000))
		Phage_0vs45<-exactTest(a,pair=c("I0","I45"))
		tags0vs45_Phage<-as.data.frame(topTags(Phage_0vs45, 5000))
		Phage_0vs60<-exactTest(a,pair=c("I0","I60"))
		tags0vs60_Phage<-as.data.frame(topTags(Phage_0vs60, 5000))
		Phage_0vs120<-exactTest(a,pair=c("I0","I120"))
		tags0vs120_Phage<-as.data.frame(topTags(Phage_0vs120, 5000))
		
		Phage_15vs30<-exactTest(a,pair=c("I15","I30"))
		tags15vs30_Phage<-as.data.frame(topTags(Phage_15vs30, 5000))	
		Phage_15vs45<-exactTest(a,pair=c("I15","I45"))
		tags15vs45_Phage<-as.data.frame(topTags(Phage_15vs45, 5000))		
		Phage_15vs60<-exactTest(a,pair=c("I15","I60"))
		tags15vs60_Phage<-as.data.frame(topTags(Phage_15vs60, 5000))		
		Phage_15vs120<-exactTest(a,pair=c("I15","I120"))
		tags15vs120_Phage<-as.data.frame(topTags(Phage_15vs120, 5000))
	
		Phage_30vs45<-exactTest(a,pair=c("I30","I45"))
		tags30vs45_Phage<-as.data.frame(topTags(Phage_30vs45, 5000))
		Phage_30vs60<-exactTest(a,pair=c("I30","I60"))
		tags30vs60_Phage<-as.data.frame(topTags(Phage_30vs60, 5000))
		Phage_30vs120<-exactTest(a,pair=c("I30","I120"))
		tags30vs120_Phage<-as.data.frame(topTags(Phage_30vs120, 5000))
			
		Phage_45vs60<-exactTest(a,pair=c("I45","I60"))
		tags45vs60_Phage<-as.data.frame(topTags(Phage_45vs60, 5000))
		Phage_45vs120<-exactTest(a,pair=c("I45","I120"))
		tags45vs120_Phage<-as.data.frame(topTags(Phage_45vs120, 5000))
			
		Phage_60vs120<-exactTest(a,pair=c("I60","I120"))
		tags60vs120_Phage<-as.data.frame(topTags(Phage_60vs120, 5000))
	  
#9. Order the DE toptags and the RPKM results the same way
   #9.1. Host I vs C
   Host_IvsC_Alltimes_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TTzero))*6) #the number of rows is the number of host genes. THe number of columns is the output of all top tags files
   rownames(Host_IvsC_Alltimes_Matrix)<-Hostgenenames
   colnames(Host_IvsC_Alltimes_Matrix)<-c(colnames(TTzero), colnames (TTfifteen),colnames(TTthirty), colnames(TTfortyfive), colnames(TTsixty), colnames(TThundredtwenty))

   TTzero<-as.matrix(TTzero) #if not converted to matrix, it has the incorrect number of dimensions and messes up with the loop
   TTfifteen<-as.matrix(TTfifteen)
   TTthirty<-as.matrix(TTthirty)
   TTfortyfive<-as.matrix(TTfortyfive)
   TTsixty<-as.matrix(TTsixty)
   TThundredtwenty<-as.matrix(TThundredtwenty)

   TTzero_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TTzero))) 
   rownames(TTzero_Matrix)<-Hostgenenames
   colnames(TTzero_Matrix)<-c(colnames(TTzero))
   TTfifteen_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TTfifteen))) 
   rownames(TTfifteen_Matrix)<-Hostgenenames
   colnames(TTfifteen_Matrix)<-c(colnames(TTfifteen))
   TTthirty_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TTthirty))) 
   rownames(TTthirty_Matrix)<-Hostgenenames
   colnames(TTthirty_Matrix)<-c(colnames(TTthirty))
   TTfortyfive_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TTfortyfive))) 
   rownames(TTfortyfive_Matrix)<-Hostgenenames
   colnames(TTfortyfive_Matrix)<-c(colnames(TTfortyfive))
   TTsixty_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TTsixty))) 
   rownames(TTsixty_Matrix)<-Hostgenenames
   colnames(TTsixty_Matrix)<-c(colnames(TTsixty))
   TThundredtwenty_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(TThundredtwenty))) 
   rownames(TThundredtwenty_Matrix)<-Hostgenenames
   colnames(TThundredtwenty_Matrix)<-c(colnames(TThundredtwenty))

   for (i in 1:length(Hostgenenames)){
   motif1<-Hostgenenames[i]   
   TTzero_Matrix[i,]<-c(TTzero[motif1,] )
   TTfifteen_Matrix[i,]<-c(TTfifteen[motif1,] )
   TTthirty_Matrix[i,]<-c(TTthirty[motif1,] )
   TTfortyfive_Matrix[i,]<-c(TTfortyfive[motif1,] )
   TTsixty_Matrix[i,]<-c(TTsixty[motif1,])
   TThundredtwenty_Matrix[i,]<-c(TThundredtwenty[motif1,])
   }
   Host_IvsC_Alltimes_Matrix<-cbind(TTzero_Matrix,TTfifteen_Matrix,TTthirty_Matrix,TTfortyfive_Matrix, TTsixty_Matrix, TThundredtwenty_Matrix)
    
#10. Between time points   
   #10.1. Host Control vs Control between time points
   
	Host_DEtimepointsCtrl_Matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags0vs15_Host))*15) 
    rownames(Host_DEtimepointsCtrl_Matrix)<-Hostgenenames
    colnames(Host_DEtimepointsCtrl_Matrix)<-c(rep(colnames(tags0vs15_Host),15))
	
   tags0vs15_Host<-as.matrix(tags0vs15_Host) #if not converted to matrix, it has the incorrect number of dimensions and messes up with the loop
   tags0vs30_Host<-as.matrix(tags0vs30_Host)
   tags0vs45_Host<-as.matrix(tags0vs45_Host)
   tags0vs60_Host<-as.matrix(tags0vs60_Host)
   tags0vs120_Host<-as.matrix(tags0vs120_Host)
   tags15vs30_Host<-as.matrix(tags15vs30_Host)
   tags15vs45_Host<-as.matrix(tags15vs45_Host)
   tags15vs60_Host<-as.matrix(tags15vs60_Host)
   tags15vs120_Host<-as.matrix(tags15vs120_Host)
   tags30vs45_Host<-as.matrix(tags30vs45_Host)
   tags30vs60_Host<-as.matrix(tags30vs60_Host)
   tags30vs120_Host<-as.matrix(tags30vs120_Host)
   tags45vs60_Host<-as.matrix(tags45vs60_Host)
   tags45vs120_Host<-as.matrix(tags45vs120_Host)
   tags60vs120_Host<-as.matrix(tags60vs120_Host)

   tags0vs15_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags0vs15_Host))) 
   rownames(tags0vs15_Host_matrix)<-Hostgenenames
   colnames(tags0vs15_Host_matrix)<-c(colnames(tags0vs15_Host))
   tags0vs30_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags0vs30_Host))) 
   rownames(tags0vs30_Host_matrix)<-Hostgenenames
   colnames(tags0vs30_Host_matrix)<-c(colnames(tags0vs30_Host))
   tags0vs45_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags0vs45_Host))) 
   rownames(tags0vs45_Host_matrix)<-Hostgenenames
   colnames(tags0vs45_Host_matrix)<-c(colnames(tags0vs45_Host))
   tags0vs60_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags0vs60_Host))) 
   rownames(tags0vs60_Host_matrix)<-Hostgenenames
   colnames(tags0vs60_Host_matrix)<-c(colnames(tags0vs60_Host))
   tags0vs120_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags0vs120_Host))) 
   rownames(tags0vs120_Host_matrix)<-Hostgenenames
   colnames(tags0vs120_Host_matrix)<-c(colnames(tags0vs120_Host))
   
   tags15vs30_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags15vs30_Host))) 
   rownames(tags15vs30_Host_matrix)<-Hostgenenames
   colnames(tags15vs30_Host_matrix)<-c(colnames(tags15vs30_Host))
   tags15vs45_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags15vs45_Host))) 
   rownames(tags15vs45_Host_matrix)<-Hostgenenames
   colnames(tags15vs45_Host_matrix)<-c(colnames(tags15vs45_Host))
   tags15vs60_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags15vs60_Host))) 
   rownames(tags15vs60_Host_matrix)<-Hostgenenames
   colnames(tags15vs60_Host_matrix)<-c(colnames(tags15vs60_Host))
   tags15vs120_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags15vs120_Host))) 
   rownames(tags15vs120_Host_matrix)<-Hostgenenames
   colnames(tags15vs120_Host_matrix)<-c(colnames(tags15vs120_Host))
   
   tags30vs45_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags30vs45_Host))) 
   rownames(tags30vs45_Host_matrix)<-Hostgenenames
   colnames(tags30vs45_Host_matrix)<-c(colnames(tags30vs45_Host))
   tags30vs60_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags30vs60_Host))) 
   rownames(tags30vs60_Host_matrix)<-Hostgenenames
   colnames(tags30vs60_Host_matrix)<-c(colnames(tags30vs60_Host))
   tags30vs120_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags30vs120_Host))) 
   rownames(tags30vs120_Host_matrix)<-Hostgenenames
   colnames(tags30vs120_Host_matrix)<-c(colnames(tags30vs120_Host))
   
   tags45vs60_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags45vs60_Host))) 
   rownames(tags45vs60_Host_matrix)<-Hostgenenames
   colnames(tags45vs60_Host_matrix)<-c(colnames(tags45vs60_Host))
   tags45vs120_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags45vs120_Host))) 
   rownames(tags45vs120_Host_matrix)<-Hostgenenames
   colnames(tags45vs120_Host_matrix)<-c(colnames(tags45vs120_Host))
   
   tags60vs120_Host_matrix<-matrix(nrow=length(Hostgenenames), ncol=length(colnames(tags60vs120_Host))) 
   rownames(tags60vs120_Host_matrix)<-Hostgenenames
   colnames(tags60vs120_Host_matrix)<-c(colnames(tags60vs120_Host))

   for (ii in 1:length(Hostgenenames)){
   motif2c<-Hostgenenames[ii]   
   tags0vs15_Host_matrix[ii,]<-c(tags0vs15_Host[motif2c,] )
   tags0vs30_Host_matrix[ii,]<-c(tags0vs30_Host[motif2c,] )
   tags0vs45_Host_matrix[ii,]<-c(tags0vs45_Host[motif2c,] )
   tags0vs60_Host_matrix[ii,]<-c(tags0vs60_Host[motif2c,] )
   tags0vs120_Host_matrix[ii,]<-c(tags0vs120_Host[motif2c,] )
   
   tags15vs30_Host_matrix[ii,]<-c(tags15vs30_Host[motif2c,] )
   tags15vs45_Host_matrix[ii,]<-c(tags15vs45_Host[motif2c,] )
   tags15vs60_Host_matrix[ii,]<-c(tags15vs60_Host[motif2c,] )
   tags15vs120_Host_matrix[ii,]<-c(tags15vs120_Host[motif2c,] )
   
   tags30vs45_Host_matrix[ii,]<-c(tags30vs45_Host[motif2c,] )
   tags30vs60_Host_matrix[ii,]<-c(tags30vs60_Host[motif2c,] )
   tags30vs120_Host_matrix[ii,]<-c(tags30vs120_Host[motif2c,] )
   
   tags45vs60_Host_matrix[ii,]<-c(tags45vs60_Host[motif2c,] )
   tags45vs120_Host_matrix[ii,]<-c(tags45vs120_Host[motif2c,] )
   tags60vs120_Host_matrix[ii,]<-c(tags60vs120_Host[motif2c,] )
}
Host_DEtimepointsCtrl_Matrix<-cbind(tags0vs15_Host_matrix,tags0vs30_Host_matrix,tags0vs45_Host_matrix, tags0vs60_Host_matrix, tags0vs120_Host_matrix,tags15vs30_Host_matrix,tags15vs45_Host_matrix,tags15vs60_Host_matrix,tags15vs120_Host_matrix, tags30vs45_Host_matrix, tags30vs60_Host_matrix, tags30vs120_Host_matrix, tags45vs60_Host_matrix,tags45vs120_Host_matrix, tags60vs120_Host_matrix)
   
   
   #10.2. Phage between time points
   Phage_DEtimepoints_Matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags0vs15_Phage))*15)  #15 is the number of DE combinations between time points
   rownames(Phage_DEtimepoints_Matrix)<-Phagegenenames_NCBI
   colnames(Phage_DEtimepoints_Matrix)<-c(rep(colnames(tags0vs15_Phage),15))
	
	# Convert the toptags to matrices
   tags0vs15_Phage<-as.matrix(tags0vs15_Phage) #if not converted to matrix, it has the incorrect number of dimensions and messes up with the loop
   tags0vs30_Phage<-as.matrix(tags0vs30_Phage)
   tags0vs45_Phage<-as.matrix(tags0vs45_Phage)
   tags0vs60_Phage<-as.matrix(tags0vs60_Phage)
   tags0vs120_Phage<-as.matrix(tags0vs120_Phage)
   
   tags15vs30_Phage<-as.matrix(tags15vs30_Phage)
   tags15vs45_Phage<-as.matrix(tags15vs45_Phage)
   tags15vs60_Phage<-as.matrix(tags15vs60_Phage)
   tags15vs120_Phage<-as.matrix(tags15vs120_Phage)
   
   tags30vs45_Phage<-as.matrix(tags30vs45_Phage)
   tags30vs60_Phage<-as.matrix(tags30vs60_Phage)
   tags30vs120_Phage<-as.matrix(tags30vs120_Phage)
   
   tags45vs60_Phage<-as.matrix(tags45vs60_Phage)
   tags45vs120_Phage<-as.matrix(tags45vs120_Phage)
   
   tags60vs120_Phage<-as.matrix(tags60vs120_Phage)

   #create the matrices that will contained the ordered toptags and gene names (each gene with its right toptap)
   tags0vs15_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags0vs15_Phage))) 
   rownames(tags0vs15_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags0vs15_Phage_matrix)<-c(colnames(tags0vs15_Phage))
   tags0vs30_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags0vs30_Phage))) 
   rownames(tags0vs30_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags0vs30_Phage_matrix)<-c(colnames(tags0vs30_Phage))
   tags0vs45_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags0vs45_Phage))) 
   rownames(tags0vs45_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags0vs45_Phage_matrix)<-c(colnames(tags0vs45_Phage))
   tags0vs60_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags0vs60_Phage))) 
   rownames(tags0vs60_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags0vs60_Phage_matrix)<-c(colnames(tags0vs60_Phage))
   tags0vs120_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags0vs120_Phage))) 
   rownames(tags0vs120_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags0vs120_Phage_matrix)<-c(colnames(tags0vs120_Phage))
   
   tags15vs30_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags15vs30_Phage))) 
   rownames(tags15vs30_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags15vs30_Phage_matrix)<-c(colnames(tags15vs30_Phage))
   tags15vs45_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags15vs45_Phage))) 
   rownames(tags15vs45_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags15vs45_Phage_matrix)<-c(colnames(tags15vs45_Phage))
   tags15vs60_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags15vs60_Phage))) 
   rownames(tags15vs60_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags15vs60_Phage_matrix)<-c(colnames(tags15vs60_Phage))
   tags15vs120_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags15vs120_Phage))) 
   rownames(tags15vs120_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags15vs120_Phage_matrix)<-c(colnames(tags15vs120_Phage))
   
   tags30vs45_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags30vs45_Phage))) 
   rownames(tags30vs45_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags30vs45_Phage_matrix)<-c(colnames(tags30vs45_Phage))
   tags30vs60_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags30vs60_Phage))) 
   rownames(tags30vs60_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags30vs60_Phage_matrix)<-c(colnames(tags30vs60_Phage))
   tags30vs120_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags30vs120_Phage))) 
   rownames(tags30vs120_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags30vs120_Phage_matrix)<-c(colnames(tags30vs120_Phage))
   
   tags45vs60_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags45vs60_Phage))) 
   rownames(tags45vs60_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags45vs60_Phage_matrix)<-c(colnames(tags45vs60_Phage))
   tags45vs120_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags45vs120_Phage))) 
   rownames(tags45vs120_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags45vs120_Phage_matrix)<-c(colnames(tags45vs120_Phage))
   
   tags60vs120_Phage_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=length(colnames(tags60vs120_Phage))) 
   rownames(tags60vs120_Phage_matrix)<-Phagegenenames_NCBI
   colnames(tags60vs120_Phage_matrix)<-c(colnames(tags60vs120_Phage))
   
	#loop to fill in those matrices
   for (i in 1:length(Phagegenenames_NCBI)){
   motif3<-Phagegenenames_NCBI[i]   
   tags0vs15_Phage_matrix[i,]<-c(tags0vs15_Phage[motif3,] )
   tags0vs30_Phage_matrix[i,]<-c(tags0vs30_Phage[motif3,] )
   tags0vs45_Phage_matrix[i,]<-c(tags0vs45_Phage[motif3,] )
   tags0vs60_Phage_matrix[i,]<-c(tags0vs60_Phage[motif3,] )
   tags0vs120_Phage_matrix[i,]<-c(tags0vs120_Phage[motif3,] )
   
   tags15vs30_Phage_matrix[i,]<-c(tags15vs30_Phage[motif3,] )
   tags15vs45_Phage_matrix[i,]<-c(tags15vs45_Phage[motif3,] )
   tags15vs60_Phage_matrix[i,]<-c(tags15vs60_Phage[motif3,] )
   tags15vs120_Phage_matrix[i,]<-c(tags15vs120_Phage[motif3,] )
   
   tags30vs45_Phage_matrix[i,]<-c(tags30vs45_Phage[motif3,] )
   tags30vs60_Phage_matrix[i,]<-c(tags30vs60_Phage[motif3,] )
   tags30vs120_Phage_matrix[i,]<-c(tags30vs120_Phage[motif3,] )
   
   tags45vs60_Phage_matrix[i,]<-c(tags45vs60_Phage[motif3,] )
   tags45vs120_Phage_matrix[i,]<-c(tags45vs120_Phage[motif3,] )
   
   tags60vs120_Phage_matrix[i,]<-c(tags60vs120_Phage[motif3,] )
   }
   Phage_DEtimepoints_Matrix<-cbind(tags0vs15_Phage_matrix, tags0vs30_Phage_matrix, tags0vs45_Phage_matrix, tags0vs60_Phage_matrix, tags0vs120_Phage_matrix,tags15vs30_Phage_matrix, tags15vs45_Phage_matrix, tags15vs60_Phage_matrix,tags15vs120_Phage_matrix,tags30vs45_Phage_matrix,tags30vs60_Phage_matrix, tags30vs120_Phage_matrix, tags45vs60_Phage_matrix,tags45vs120_Phage_matrix,tags60vs120_Phage_matrix )
 	

#11. Additional info:

   #11.1. Phage gene name, function, start and stop
   Phage_geneCoorNfunc_matrix<-matrix(nrow=length(Phagegenenames_NCBI), ncol=3) #matrix that will hold the gene name, function, start and stop (but name is already set with row names)
   rownames(Phage_geneCoorNfunc_matrix)<-Phagegenenames_NCBI #has the genes of host and phage w/o DE order
   colnames(Phage_geneCoorNfunc_matrix)<-c("Function", "Start", "Stop")
   Phage_geneCoorNfunc_matrix[,1]<-c(Phage_NCBI[,11]) #obtain gene function 
   Phage_geneCoorNfunc_matrix[,2]<-c(Phage_NCBI[,3]) #obtain gene start 
   Phage_geneCoorNfunc_matrix[,3]<-c(Phage_NCBI[,4]) #obtain gene stop

   #11.2. Host gene name, function, start and stop
   Host_geneCoorNfunc_matrix<-matrix(nrow=length(Hostgenenames), ncol=3) #matrix that will hold the gene name, function, start and stop
   rownames(Host_geneCoorNfunc_matrix)<-Hostgenenames #has the genes of host and phage w/o DE order
   colnames(Host_geneCoorNfunc_matrix)<-c("Function", "Start", "Stop")
   Host_geneCoorNfunc_matrix[,1]<-c(hostgenes[,7]) #obtain gene function
   Host_geneCoorNfunc_matrix[,2]<-c(hostgenes[,3]) #obtain gene start  
   Host_geneCoorNfunc_matrix[,3]<-c(hostgenes[,4]) #obtain gene stop   


#12. Put everything together into the final file for analysis:
	
		Phage_final_geneinfoDEandRPKM_IvsI<-cbind(Phage_geneCoorNfunc_matrix, RPKM_Phage_removed_norm, Phage_DEtimepoints_Matrix)
		
		Host_final_DEandRPKM_dupl_CvsC<-cbind(Host_geneCoorNfunc_matrix, RPKM_Host_removed_norm_C, Host_DEtimepointsCtrl_Matrix)
		
		Host_final_DEandRRPKM_dupl_IvsC<-cbind(Host_geneCoorNfunc_matrix, RPKM_Host_removed_norm, Host_IvsC_Alltimes_Matrix)
		
savehistory()
