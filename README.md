# README #

R scripts used in "Regulating infection efficiency in a globally abundant marine Bacteriodetes virus", for the analysis of transcriptomes from phage-host time-series samples.