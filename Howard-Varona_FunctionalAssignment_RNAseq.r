#Functional group assignment to differentially expressed host genes - Cristina Howard-Varona 

setwd("Directory1/Directory1.1/") #Set your directory to where the files with genes in need of function are


#1. Read and store the file with the gene names and assigned categories
FunctionalGroups_forNCBI<-read.delim("Directory1/Directory1.1/nameOfFile.csv", sep=",", header=T, stringsAsFactors = F)
FunctionalGroups_forNCBI<-as.matrix(FunctionalGroups_forNCBI) #there are 4 cols: Function, Category, Subcategory, Subsystem
 
 
#2. Assign functional categories to genes

	#For 18 (infected with 38:1)
	Host18DEgenesRPKM<-read.csv("Directory1/Directory1.1/nameOfFile.csv", sep=",", header=T, stringsAsFactors = F) 
	Host18DEgenesRPKM<-as.matrix(Host18DEgenesRPKM) #col 2 has gene IDs, col 3 has functions
	
	matchedfunctionMatrix=matrix(nrow=nrow(Host18DEgenesRPKM), ncol=ncol(Host18DEgenesRPKM)+ncol(FunctionalGroups_forNCBI))
	rownames(matchedfunctionMatrix)<-rownames(Host18DEgenesRPKM)
	colnames(matchedfunctionMatrix) = c(colnames(Host18DEgenesRPKM), colnames(FunctionalGroups_forNCBI))
	matchedfunctionMatrix[,1:13]<-Host18DEgenesRPKM[,]

	for (j in 1:nrow(Host18DEgenesRPKM)){
	figID<-Host18DEgenesRPKM[j,3] #obtains just the function of the gene, to find it in the subsystems file 
	correspondingsubsystem<-FunctionalGroups_forNCBI[match(figID, FunctionalGroups_forNCBI),]  #match(figID, FunctionalGroups_SR) will give the line number in which the pattern figID is found
	matchedfunctionMatrix[j,14:ncol(matchedfunctionMatrix)] <-correspondingsubsystem
	}
	

#Annotate better the "NAs"	
	for (i in 2:nrow(matchedfunctionMatrix)){
	  assignedCategory<-matchedfunctionMatrix[i,16]
	  #cat ("iteration", i, "\n")
	  if (identical(assignedCategory[[1]], NA_character_)) {
		 functionID<-matchedfunctionMatrix[i,3]
		 grepnumber<-grep(functionID, FunctionalGroups_forNCBI[,1])
		 
		 if (length(grepnumber) == 1) {
		    refinedsubsystem1<-FunctionalGroups_forNCBI[grep(functionID, FunctionalGroups_forNCBI[,1]),]
			matchedfunctionMatrix[i,14:ncol(matchedfunctionMatrix)]<-refinedsubsystem1
		 }
	   }
	 } 
	
	
	#For 38
	
	Host38DEgenesRPKM<-read.delim("Directory1/Directory1.1/nameOfFile.csv", sep=",", header=T, stringsAsFactors = F) #contains all the time points (ordered vertically, one after the other) and by columns, the gene ID, Function, Start, Stop, log2RPKMAvC, log2RPKMAvI, logFC
	Host38DEgenesRPKM<-as.matrix(Host38DEgenesRPKM)
	
	matchedfunctionMatrix=matrix(nrow=nrow(Host38DEgenesRPKM), ncol=ncol(Host38DEgenesRPKM)+ncol(FunctionalGroups_forNCBI))
	rownames(matchedfunctionMatrix)<-rownames(Host38DEgenesRPKM)
	colnames(matchedfunctionMatrix) = c(colnames(Host38DEgenesRPKM), colnames(FunctionalGroups_forNCBI))
	matchedfunctionMatrix[,1:11]<-Host38DEgenesRPKM[,] #there are 11 columns in the file 141203Host38DEfunctions_AllTs

	for (f in 1:nrow(Host38DEgenesRPKM)){
	figID<-Host38DEgenesRPKM[f,3] #obtains just the function of the gene, to find it in the subsystems file 
	correspondingsubsystem<-FunctionalGroups_forNCBI[match(figID, FunctionalGroups_forNCBI),]  #match(figID, FunctionalGroups_SR) will give the line number in which the pattern figID is found
	matchedfunctionMatrix[f,12:ncol(matchedfunctionMatrix)] <-correspondingsubsystem
	}
		
	
	#try to annotate better the "NAs"	
	
	matchedfunctionMatrix<-read.csv("Directory1/Directory1.1/nameOfFile.csv", sep=",", header=T, stringsAsFactors = F) #contains all the time points (ordered vertically, one after the other) and by columns, the gene ID, Function, Start, Stop, log2RPKMAvC, log2RPKMAvI, logFC
	matchedfunctionMatrix<-as.matrix(matchedfunctionMatrix)
		
	for (i in 2:nrow(matchedfunctionMatrix)){
	  assignedCategory<-matchedfunctionMatrix[i,13]  # the categories are in col 13 now
	  #cat ("iteration", i, "\n")
	  if (identical(assignedCategory[[1]], NA_character_)) {    #if the category is labeled as 'NA'
		 functionID<-matchedfunctionMatrix[i,3]   #the gene function is in col 3
		 grepnumber<-grep(functionID, FunctionalGroups_forNCBI[,1])   #in how many rows is the function?
		 
		 if (length(grepnumber) == 1) {  #proceed only if there is 1 row/function (multiple entries for the same function confuse the program and do not assign it to a gene)
		    refinedsubsystem1<-FunctionalGroups_forNCBI[grep(functionID, FunctionalGroups_forNCBI[,1]),]
			matchedfunctionMatrix[i,12:ncol(matchedfunctionMatrix)]<-refinedsubsystem1  #we want to replace col 12-14 with the contents from the FunctionalGroups_forNCBI file
		 }
	   }
	 } 
		
	
